import logging
from logging import info
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename='bot.log')
from telegram.ext import Updater, Defaults, CallbackContext, CommandHandler, MessageHandler, ConversationHandler, Filters
from telegram import Update, Bot
import telegram
import sys
import platform
import threading
import time
import schedule 
class Homebot:
    def __init__(self):
        self.bot = Updater('1470615684:AAG1A6VVryqBgRdPnre4rCZLjl16BoVc6Jw')
        self.dispatcher = self.bot.dispatcher
        self.dispatcher.add_handler(CommandHandler('start', self.start))
        self.dispatcher.add_handler(CommandHandler('help', self.help))
        self.dispatcher.add_handler(CommandHandler('pyinfo', self.pyinfo))
        self.dispatcher.add_handler(CommandHandler('srvinfo', self.srvinfo))
        self.dispatcher.add_handler(MessageHandler(Filters.text('1'), self.pasxalka1))
        self.dispatcher.add_handler(MessageHandler(Filters.text('2'), self.pasxalka2))
        self.dispatcher.add_handler(MessageHandler(Filters.text('3'), self.pasxalka3))
        self.usersToNotify = set()
        self.scheduler = threading.Thread(target=self.StartScheduling)
        self.scheduler.start()


 #   def echo(self,update:Update, context:CallbackContext):
 #       logging.info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
 #       update.message.reply_text(
 #           update.message.text
 #       )

    def pasxalka1(self,update:Update,CallbackContext):
        logging.info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
        update.message.reply_photo(
            photo='https://i.ytimg.com/vi/JY9APJkfVow/maxresdefault.jpg'
        )
    def pasxalka2(self,update:Update,CallbackContext):
        logging.info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
        with open('пасхалка.txt') as f:
            update.message.reply_document(
            f,
            )
    def pasxalka3(self,update:Update,CallbackContext):
        logging.info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
        with open('пасхалка.mp3', 'rb') as f:
            update.message.reply_audio(
            f,
            )
    def SendDiceToUsers(self):
        info('Notify task started')
        for userToNotify in self.usersToNotify:
            temp = Bot(token='1470615684:AAG1A6VVryqBgRdPnre4rCZLjl16BoVc6Jw')
            temp.sendDice(userToNotify,emoji='🎰')
            info('Notify task ')

    def start(self, update:Update, context:CallbackContext):
        logging.info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
        self.usersToNotify.add(update.message.from_user.id)
        update.message.reply_text(
           " бот с домашним заданием"
        )
    def StartScheduling(self):
        info('Initializing scheduler...')
        schedule.every(1).seconds.do(self.SendDiceToUsers)
        while True:
            schedule.run_pending()
            time.sleep(1)
    def help(self, update:Update, context:CallbackContext):
        logging.info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
        update.message.reply_text("потерялся?")
        update.message.reply_text("есть комманды: start, help, pyinfo и srvinfo. И еще есть 3 пасхалки ")
    def pyinfo(self, update:Update, context:CallbackContext):
        sys.version[:3]
        logging.info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
        update.message.reply_text(
         'этот бот в данный момент работает на python' + sys.version[:3]
        )

    def srvinfo(self, update:Update, context:CallbackContext):
        logging.info(f'Got message from user { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id }) - "{ update.message.text }"')
        update.message.reply_text(
         'system info'
        )
        a = 'разрадность машины:\n' + str(platform.machine())
        c = 'os машины:\n' + str(platform.platform())
        g = 'процессор машины:\n' + str(platform.processor())
        update.message.reply_text(
        a + '\n' + c + '\n' + g
        )
        

    def run(self):
        info('Starting bot...')
        self.bot.start_polling()
        self.bot.idle()

if __name__ == "__main__":
        bot = Homebot()
        bot.run()
